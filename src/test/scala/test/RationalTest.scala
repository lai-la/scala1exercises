package test

import exercise1.Rational
import org.scalatest.FunSuite

//see http://www.scalatest.org/at_a_glance/FunSuite
class RationalTest extends FunSuite {

  val r1 = new Rational(3,4)
  val r2 = new Rational(1,2)
  
  test("Rational Inititalisation 1") {
    val x = new Rational(1,2)
    assert(x.value === 0.5)
  }
  
  test("Rational Inititalisation 2") {
    val x = new Rational(1,2)
    assertResult("1/2"){x.toString}
  }

  test("Test requirement (denominator!=0)"){
      intercept [IllegalArgumentException] {
        new Rational(1,0)}
  }

  test("Test add"){
    val res = r1 add r2
    assert(res.toString === "10/8")
  }
  
  test("Test sub"){
    val res = r1 sub r2
    assert(res.toString === "2/8")
  }

  test("Test mul") {
    val res = r1 * r2
    assert(res.toString === "3/8")
  }

  test("Test neg"){
    val res = r1.neg()
    assert(res.toString === "-3/4")
  }

}

