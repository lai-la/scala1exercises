package exercise1

class Rational(numerator: Int, denominator: Int) {

  def this(denom: Int) = this(1, denom)

  override def toString: String = numerator + "/" + denominator

  require(denominator != 0, "Denominator muss != 0 sein") // throws a IllegalArgumentException if the condition is not met
  println("Eine Rationale Zahl wurde erzeugt....") // is part of the constructor

  def num: Int = numerator // so the numerator is accessible from other classes
  def denom: Int = denominator // so the numerator is accessible from other classes
  def value: Double = num.toDouble / denom // converts to floating point number

  def max(other: Rational): Rational = {
    if (numerator / denominator < other.num / other.denom) this else other
  }

  def add(other: Rational): Rational = new Rational(num * other.denom + denom * other.num, denom * other.denom)


  def mul(other: Rational): Rational = new Rational(num * other.num, denom * other.denom)

  def sub(other: Rational): Rational = {
    if(denom == other.denom) new Rational(num - other.num, denom) else new Rational(num * other.denom - denom * other.num, denom * other.denom)

  }

  def neg(): Rational = new Rational(-num, denom)


  /* Using symbolic definitions */
  def *(other: Rational): Rational = mul(other)

  def +(other: Rational): Rational = add(other)

  def -(other: Rational): Rational = sub(other)

}
